﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeController : MonoBehaviour
{
    public enum BridgeState
    {
        Wait, BridgeUP, BridgeFall,PlayAgain

    }
    public BridgeState bridgestate;
    public float scaleUp;
    GameObject childGameObject;
    bool isBridgeStayZ_Axis;
    public int speed;
    public PlayerManager player;
    Vector3 startposition;

    private void Awake()
    {
        startposition = transform.position;
        isBridgeStayZ_Axis = true;
        childGameObject = gameObject.transform.GetChild(0).gameObject;
    }

    void Start()
    {
        GameManager.GameStartEvent += OnGameStarted;
        GameManager.GameFinishEvent += OnGameFinished;
    }

    private void OnGameFinished()
    {
        ChangeBridgeState(BridgeState.PlayAgain);
    }

    private void OnGameStarted()
    {
        isBridgeStayZ_Axis = true;
        StartCoroutine(ExecuteAfterTime(1));
    }

    void SetBridgeProperties()
    {
        Vector3 position = player.transform.position;
        if (isBridgeStayZ_Axis)
        {
            position.z += 0.6f;
            transform.localScale = new Vector3(1, 0, 0.2f);
        }
        else
        {
            position.x -= 0.6f;
            transform.localScale = new Vector3(0.2f, 0, 1);
        }
        position.y -= 0.5f;
        transform.position = position;
        transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        ChangeBridgeState(BridgeState.BridgeUP);
    }

    private void ChangeBridgeState(BridgeState bridgestate)
    {
        this.bridgestate = bridgestate;
    }


    void Update()
    {
        switch (bridgestate)
        {
            case BridgeState.Wait:
                break;
            case BridgeState.BridgeUP:
                if (Input.GetMouseButton(0))
                {
                    transform.localScale += new Vector3(0, scaleUp, 0);
                }
                if (Input.GetMouseButtonUp(0))
                {
                    ChangeBridgeState(BridgeState.BridgeFall);
                }
                break;
            case BridgeState.BridgeFall:
                if (isBridgeStayZ_Axis)
                {
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(90, 0, 0), speed * Time.deltaTime);
                }
                else
                {
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, 0, 90), speed * Time.deltaTime);
                }
                if (transform.rotation.eulerAngles.x == 90f || transform.rotation.eulerAngles.z == 90f)
                    CrossBridge();
                break;
            case BridgeState.PlayAgain:
                SetBridgeProperties();
                break;
        }
    }

    public void PlayerPassed()
    {
        SetBridgeProperties();
        StartCoroutine(ExecuteAfterTime(1));
    }

    private void CrossBridge()
    {
        ChangeBridgeState(BridgeState.Wait);
        GameManager.instance.CrossBridge(childGameObject.transform.position, childGameObject.transform.lossyScale.y * 0.5f, isBridgeStayZ_Axis);
        isBridgeStayZ_Axis = !isBridgeStayZ_Axis;
    }
}