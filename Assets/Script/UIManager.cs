﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject StartPanel;
    public Text StartText;
    public GameObject FinishPanel;
    public Text FinishText;

    private void Awake()
    {
        StartText.text = "Tap To Start";
        FinishText.text = "Play Again";
    }

    void Start()
    {
        GameManager.GameStartEvent += OnGameStarted;
        GameManager.GameFinishEvent += OnGameFinished;
    }

    private void OnGameFinished()
    {
        OpenFinishPanel();
    }

    private void OpenFinishPanel()
    {
        StartPanel.SetActive(false);
        FinishPanel.SetActive(true);
    }

    private void OnGameStarted()
    {        
        CloseStartPanel();
    }

    private void CloseStartPanel()
    {
        StartPanel.SetActive(false);
        FinishPanel.SetActive(false);
    }
}
