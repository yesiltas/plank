﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static Action GameStartEvent;
    public static Action GameFinishEvent;
    public static GameManager instance;
    public bool isPlayerStay = true;
    public PlayerManager player;
    public BridgeController bridge;
    public PlaneManager plane;
    public CameraManger camera;
    Vector3 targetPosition;

    private void Awake()
    {
        instance = this;
    }

    public void StartGame()
    {
        GameStartEvent();
    }

    void FinishGame()
    {
        GameFinishEvent();
    }

    public void TargetPosition(Vector3 targetPosition)
    {
        this.targetPosition = targetPosition;
        player.TargetPosition(this.targetPosition);
    }

    public void CrossBridge(Vector3 bridgePositon, float bridgeSize, bool isBridgeStayZ_Axis)
    {
        bool isCrossBridge = true;
        if (isBridgeStayZ_Axis)
        {
            float max_y = (targetPosition.z + 2) - bridgePositon.z;
            float min_y = (targetPosition.z - 2) - bridgePositon.z;
            if (!(bridgeSize > min_y && bridgeSize < max_y))
            {
                player.NotCrossBridge(bridgeSize * 2);
                isCrossBridge = false;
            }
        }
        else
        {
            float min_y = (targetPosition.x + 2) - bridgePositon.x;
            float max_y = (targetPosition.x - 2) - bridgePositon.x;
            if (!(bridgeSize > -min_y && bridgeSize < -max_y))
            {
                player.NotCrossBridge(bridgeSize * 2);
                isCrossBridge = false;
            }
        }
        if (isCrossBridge)
           player.OnPlayerMove();
    }

    public void PlayerPassed()
    {
        bridge.PlayerPassed();
        plane.InitializePlane();
    }

    public void PlayerDie()
    {
        FinishGame();
    }
}
