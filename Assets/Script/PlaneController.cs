﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneController : MonoBehaviour
{
    public virtual void Initialize(Vector3 position)
    {
        transform.position = position;
        OnInitialize();
    }

    public virtual void OnInitialize()
    {
        gameObject.SetActive(true);
    }
}
