﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    Vector3 targetPostion;
    float bridgeSize;
    Vector3 startPosition;
    bool movementZ_Axis;
    public enum PlayerState
    {
        Wait, Move, Die
    }
    public PlayerState playerState;

    private void Awake()
    {
        movementZ_Axis = true;
        startPosition = transform.position;
    }

    void Start()
    {
        GameManager.GameStartEvent += OnGameStarted;
    }

    private void OnGameStarted()
    {
        transform.position = startPosition;
        movementZ_Axis = true;
    }

    private void OnPlayerStay()
    {
        playerState = PlayerState.Wait;
    }

    public void OnPlayerMove()
    {
        playerState = PlayerState.Move;
    }

    void Update()
    {
        switch (playerState)
        {
            case PlayerState.Wait:
                break;
            case PlayerState.Move:
                if (transform.position.z < targetPostion.z || transform.position.x > targetPostion.x)
                {
                    if (movementZ_Axis)
                        transform.position += new Vector3(0, 0, 0.1f);
                    else
                        transform.position += new Vector3(-0.1f, 0, 0);
                }
                else
                {
                    GameManager.instance.PlayerPassed();
                    movementZ_Axis = !movementZ_Axis;
                    OnPlayerStay();
                }
                break;
            case PlayerState.Die:
                PlayerDie();
                break;
        }
    }
    private void PlayerDie()
    {
        if (movementZ_Axis)
        {
            if (transform.position.z < bridgeSize)
            {
                transform.position += new Vector3(0, 0, 0.1f);
            }
            else
                transform.position += new Vector3(0, -0.1f, 0);
        }
        else
        {
            if (-transform.position.x < bridgeSize)
            {
                transform.position += new Vector3(-0.1f, 0, 0);
            }
            else
                transform.position += new Vector3(0, -0.1f, 0);
        }
        if (transform.position.y < -5)
        {
            GameManager.instance.PlayerDie();
            OnPlayerStay();
        }
    }

    public void NotCrossBridge(float bridgeSize)
    {
        playerState = PlayerState.Die;
        this.bridgeSize = bridgeSize;
    }

    public void TargetPosition(Vector3 targetPosition)
    {
        this.targetPostion = targetPosition;
    }
}


