﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneManager : MonoBehaviour
{
    public GameObject prefab;
    float distance;
    Vector3 basePosition;
    bool isStageUp;
    List<PlaneController> planeList = new List<PlaneController>();

    private void Awake()
    {
        SetPlaneProperties();      
    }

    private void Start()
    {       
        GameManager.GameStartEvent += OnGameStarted;
        GameManager.GameFinishEvent += OnGameFinished;
    }

    private void OnGameFinished()
    {
        DestroyPlanes();
        SetPlaneProperties();
    }

    private void DestroyPlanes()
    {
        foreach (var item in planeList)
        {
            Destroy(item.gameObject);
        }
        planeList.Clear();
    }

    private void OnGameStarted()
    {
        InitializePlane();
        InitializePlane();
    }

    void SetPlaneProperties()
    {
        distance = 6f;
        basePosition = new Vector3(0.5f, -2.5f, 0);
    }

    public void InitializePlane()
    {
        PlaneController plane = Instantiate(prefab).GetComponent<PlaneController>();
        plane.Initialize(SetPlanePosition(basePosition));
        planeList.Add(plane);
    }

    Vector3 SetPlanePosition(Vector3 positon)
    {
        if (positon.z > -positon.x)
        {
            positon.x = -positon.z;
            basePosition = positon;
            GameManager.instance.TargetPosition(basePosition);
            distance += 0.5f;
        }
        else
        {
            if (isStageUp)
            {
                SetStageDistance();
            }
            positon.z += distance;
            basePosition = positon;
            GameManager.instance.TargetPosition(basePosition);
            distance += 0.5f;
        }
        return positon;
    }

    private void SetStageDistance()
    {
        distance = distance + 2f;
        isStageUp = false;
    }
}
